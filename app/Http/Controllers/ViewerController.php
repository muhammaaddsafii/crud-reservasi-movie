<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Viewer;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class ViewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewers = Viewer::get();
        $movies = Movie::get();
        return view('viewer.index', [
            'movies' => $movies,
            'viewers' => $viewers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Viewer $viewers)
    {
        $movies = Movie::get();
        return view('viewer.edit', [
            'viewers' => $viewers,
            'movies' => $movies
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Viewer $viewers)
    {
        $attr['name'] = request('name');
        $viewers->movies()->sync(request('movies'));

        $viewers->update($attr);

        return redirect('/viewer')->with('status', 'Update success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Viewer $viewers)
    {
        $viewers->movies()->detach();
        $viewers->delete();

        return redirect('/viewer')->with('status', 'Delete Success !');
    }
}
