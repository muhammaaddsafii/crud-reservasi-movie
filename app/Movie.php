<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['name', 'slug', 'description'];

    public function viewers()
    {
        return $this->belongsToMany(Viewer::class);
    }
}
