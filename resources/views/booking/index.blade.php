@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Isi Form Untuk Booking Tiket</h4>
            <form action="/booking/store" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Nama :</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="form-group mt-3">
                    <label for="movies">Pilih Movie :</label>
                    <select name="movies[]" class="form-control select2" required multiple>
                        @foreach ($booking as $book)
                        <option value="{{$book->id}}"> {{$book->name}} </option>
                        @endforeach
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Booking</button>
            </form>
        </div>
    </div>
</div>
@endsection
