@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Isi Form Untuk Tambah Movie</h4>
            <form action="/movie/store" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Judul Movie :</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="description">Description :</label>
                    <input type="text" name="description" id="description" class="form-control" required>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Tambah</button>
            </form>
        </div>
    </div>
</div>
@endsection
