@extends('layout.master')

@section('content')
<div class="container">
    <div>
        <h4 class="text-secondary">List Movie</h4>
    </div>
    <div class="d-flex justify-content-end">
        <a class="btn btn-warning rounded-pill mb-4" href="/movie/create">Tambah Data Movie</a>
    </div>
    <div class="row">
        @foreach ($movies as $movie)
        <div class="col-4 mb-5">
            <div class="card" style="">
                <div class="card-body">
                <h5 class="card-title">{{ $movie->name}}</h5>
                <p class="card-text">{{ $movie->description }}</p>
                <a href="/booking" class="btn btn-primary rounded-pill">Book</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
