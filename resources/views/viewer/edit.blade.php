@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Isi Form Untuk Edit Viewer</h4>
            <form action="/viewer/{{ $viewers->slug }}/edit" method="POST" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <label for="name">Nama :</label>
                    <input type="text" name="name" value="{{ old('name') ?? $viewers->name }}" id="name" class="form-control">
                </div>
                <div class="form-group mt-3">
                    <label for="movies">Pilih Movie :</label>
                    <select name="movies[]" class="form-control select2" multiple>
                        @foreach ($viewers->movies as $movie)
                        <option selected value="{{$movie->id}}"> {{$movie->name}} </option>
                        @endforeach
                        @foreach ($movies as $movie)
                        <option value="{{$movie->id}}"> {{$movie->name}} </option>
                        @endforeach
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Booking</button>
            </form>
        </div>
    </div>
</div>
@endsection
