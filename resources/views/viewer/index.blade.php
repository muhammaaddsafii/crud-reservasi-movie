@extends('layout.master')

@section('content')
<div class="container">
    <div>
        <h4 class="text-secondary mb-5">List Viewer</h4>
    </div>
    <div class="row">
        @foreach ($viewers as $viewer)
        <div class="col-4 mb-5">
            <div class="card" style="width: 18rem;">
                <div class="card-header text-wrap">
                Nama Viewer : {{ $viewer->name }}
                </div>
                @foreach ($viewer->movies as $movie)
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{ $movie->name }}</li>
                </ul>
                @endforeach
                <div class="card-footer d-flex justify-content-between">
                    <a class="btn btn-warning btn-sm rounded-pill" href="/viewer/{{ $viewer->slug }}/edit">Edit</a>
                    <form action="/viewer/{{ $viewer->slug }}/delete" method="POST">
                    @csrf
                    @method('delete')
                    <div>
                        <button type="submit" class="btn btn-danger btn-sm rounded-pill">Delete</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
