<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@index');

Route::get('/booking', 'BookingController@index');
Route::post('/booking/store', 'BookingController@store');

Route::get('/movie', 'MovieController@index');
Route::get('/movie/create', 'MovieController@create');
Route::post('/movie/store', 'MovieController@store');

Route::get('/viewer', 'ViewerController@index');
Route::get('/viewer/{viewers:slug}/edit', 'ViewerController@edit');
Route::patch('/viewer/{viewers:slug}/edit', 'ViewerController@update');
Route::delete('/viewer/{viewers:slug}/delete', 'ViewerController@destroy');
