<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = collect([
            'Laskar Pelangi', 'Ketika Cinta Bertasbih', 'NKCTHI', 'Doraemon Movie 3', 'Avenger:End Game', 'Harry Potter',
        ]);
        $movies->each(function ($m) {
            \App\Movie::create([
                'name' => $m,
                'slug' => Str::slug($m),
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, temporibus.'
            ]);
        });
    }
}
