<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieViewerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_viewer', function (Blueprint $table) {
            $table->foreignId('movie_id')->constrained('movies');
            $table->foreignId('viewer_id')->constrained('viewers');

            $table->primary(['movie_id', 'viewer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_viewer');
    }
}
